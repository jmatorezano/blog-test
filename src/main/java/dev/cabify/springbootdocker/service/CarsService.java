package dev.cabify.springbootdocker.service;

import dev.cabify.springbootdocker.entity.Journey;
import dev.cabify.springbootdocker.entity.JourneyCar;
import dev.cabify.springbootdocker.entity.TypeCar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class CarsService {

    @Autowired
    private JourneyCar journeyCar;

    Boolean assigned = true;

    Boolean droppedOff=false;

    @Autowired
    Journey journey;

    private List<TypeCar> typeCarsMemory = new LinkedList<TypeCar>();

    private Queue<Journey> queue = new LinkedList<Journey>();

    Map<Long, JourneyCar> journeysassigned = new ConcurrentHashMap<>();




    public void saveCars (List <TypeCar> typeCars) {

        if (!typeCarsMemory.isEmpty()) {
            typeCarsMemory.clear();
            journeysassigned.clear();
            queue.clear();
        }

        for (TypeCar typeCar:typeCars) {
            typeCarsMemory.add(typeCar);
        }
    }

    public void saveJourney (Journey journey) {

      queue.offer(journey);


    }


    public boolean dropOff (Long id) {

       if (dropOffQueue(id)) {
            return true;
        }
       else {

           return dropOffJourney(id);
       }
    }

    public boolean dropOffQueue(Long id) {

        droppedOff = (queue.stream().anyMatch(p -> id == p.getId()) || journey.getId() == id );
        queue.removeIf(p -> id == p.getId());

        return droppedOff ;
    }

    public boolean dropOffJourney(Long id) {

        if (journeysassigned.containsKey(id)) {
            JourneyCar journeyCar = journeysassigned.get(id);
            TypeCar typeCar = journeyCar.getTypecar();
            Journey journey= journeyCar.getJourney();
            typeCar.setSeats(typeCar.getSeats()+journey.getPeople());
            //addFreeSeatsCar (typeCar,typeCarsMemory);
            journeysassigned.remove(id);
            return true;
        }

        return false;
    }

    public boolean locateQueue(Long id) {

         return (queue.stream().anyMatch(p -> id == p.getId())|| journey.getId() == id );

    }

    public boolean locateJourney(Long id) {

        if (journeysassigned.containsKey(id)) {
            return true;
        }

        return false;
    }


    public void doService() {


        while (!queue.isEmpty()) {

            journey = queue.poll();

            assigned = assingJourneyToCar(typeCarsMemory, journey);

            if (!assigned && !droppedOff)
                queue.offer(journey);
        }
    }



    public boolean assingJourneyToCar(List<TypeCar> typeCars, Journey journey) {


        for (TypeCar typeCar : typeCars) {
            if (typeCar.getSeats() >= journey.getPeople()) {
                typeCar.setSeats(typeCar.getSeats() - journey.getPeople());
                saveJourney(journey, typeCar);
                return true;
            }

        }
        return false;
    }

    private void saveJourney(Journey journey, TypeCar typeCar) {
        JourneyCar journeyCar = new JourneyCar(journey, typeCar);
        journeysassigned.put(journey.getId(), journeyCar);
    }

    private void addFreeSeatsCar (TypeCar typeCar, List<TypeCar> typeCars) {
        typeCars.removeIf(p -> typeCar.getId().equals(p.getId()));
        typeCars.add(typeCar);
    }

    public JourneyCar getJourneyCar() {
        return journeyCar;
    }

    public void setJourneyCar(JourneyCar journeyCar) {
        this.journeyCar = journeyCar;
    }
}
