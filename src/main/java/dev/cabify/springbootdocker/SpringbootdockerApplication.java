package dev.cabify.springbootdocker;

import dev.cabify.springbootdocker.service.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SpringbootdockerApplication {


	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(SpringbootdockerApplication.class, args);
		CarsService carsService = applicationContext.getBean(CarsService.class);

		while (true) {
			carsService.doService();
		}



	}


}
