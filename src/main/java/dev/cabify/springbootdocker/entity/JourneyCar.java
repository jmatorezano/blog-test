package dev.cabify.springbootdocker.entity;

import org.springframework.stereotype.Component;

@Component
public class JourneyCar {

    private Journey journey;

    private TypeCar typecar;


    public Journey getJourney() {
        return journey;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

    public TypeCar getTypecar() {
        return typecar;
    }

    public void setTypecar(TypeCar typecar) {
        this.typecar = typecar;
    }

    public JourneyCar(Journey journey, TypeCar typecar) {
        this.journey = journey;
        this.typecar = typecar;
    }
}
