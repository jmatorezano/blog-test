package dev.cabify.springbootdocker.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Component
public class TypeCar {

    @JsonProperty("id")
    @NotNull
    private Long id;

    @JsonProperty("seats")
    @Min(value = 1)
    @Max(value = 6)
    @NotNull
    private Integer seats;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public TypeCar() {
    }
}
