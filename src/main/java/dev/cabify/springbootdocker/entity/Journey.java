package dev.cabify.springbootdocker.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Component
public class Journey {

    @JsonProperty("id")
    @NotNull
    private Long id;

    @JsonProperty("people")
    @Min(value = 1)
    @Max(value = 6)
    @NotNull
    private Integer people;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPeople() {
        return people;
    }

    public void setPeople(Integer people) {
        this.people = people;
    }

    public Journey() {
    }
}
