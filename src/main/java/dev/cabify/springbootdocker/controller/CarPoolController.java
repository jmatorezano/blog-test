package dev.cabify.springbootdocker.controller;



import dev.cabify.springbootdocker.entity.Journey;
import dev.cabify.springbootdocker.entity.TypeCar;
import dev.cabify.springbootdocker.service.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
public class CarPoolController {

    @Autowired
    private CarsService carsService;


    @RequestMapping("/")
    public String home() {
        return "Hello Spring Boot with Docker";
    }


    @GetMapping("/status")
    public ResponseEntity status() {
        return ResponseEntity.ok().build();

    }

    @PutMapping(value = "/cars",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity updateCars(@NotNull @RequestBody List<TypeCar> typeCars) {

        if (typeCars.isEmpty())
            return ResponseEntity.badRequest().build();

        carsService.saveCars(typeCars);

        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/journey",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity journey(@NotNull @RequestBody Journey journey) {

        carsService.saveJourney(journey);

        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/dropoff",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity dropoff ( @RequestParam("id") String id)  {

        if (!carsService.dropOff(Long.parseLong(id))) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping(
            path = "/locate",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity locate ( @RequestParam("id") String id)  {

        if (carsService.locateJourney(Long.parseLong(id))) {
            return  ResponseEntity.ok().build();
        }
        else if (carsService.locateQueue(Long.parseLong(id))) {
            return ResponseEntity.noContent().build();
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

}
