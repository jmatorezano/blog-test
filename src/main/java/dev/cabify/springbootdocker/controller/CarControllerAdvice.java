package dev.cabify.springbootdocker.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.MediaTypeNotSupportedStatusException;

@RestControllerAdvice
public class CarControllerAdvice {

    @ExceptionHandler(MediaTypeNotSupportedStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleNonExisting() {
    }

}
